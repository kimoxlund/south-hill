// if ('serviceWorker' in navigator) {

//   navigator.serviceWorker
//     .register('/sw.js', { scope: '/' })
//     .then(function(registration) {
//       console.log("Service Worker Registered");
//     })
//     .catch(function(err) {
//       console.log("Service Worker Failed to Register", err);
//     })
// }

(function () {
  "use strict";

  // Toggle Nav-bar
  $(".c-nav__toggle").on("click", function () {
    $("body").toggleClass("nav-open");
  });

  $(".c-nav__link").on("click", function (event) {
    $("body").removeClass("nav-open");
  });

  // Form labels.
  // $('.form ').find('input').on('focus', function() {
  //    $(this).parent().addClass('has-focus');
  // });

  $(".form")
    .find("input, textarea, select")
    .on("click change keyup paste load", function () {
      var value = $(this).val();

      $(".form-segment").removeClass("has-focus");

      if ($(this).is(":focus")) {
        $(this).closest(".form-segment").addClass("has-focus");
      }

      if (value !== "") {
        $(this).next(".placeholder__text").addClass("is-hidden");
      } else {
        $(this).next(".placeholder__text").removeClass("is-hidden");
      }
    });

  $(".form")
    .find("input, textarea, select")
    .on("focusout", function () {
      var value = $(this).val();

      if (value !== "") {
        $(this).closest(".form-segment").addClass("has-value");
        $(this).next(".placeholder__text").addClass("is-hidden");
      } else {
        $(this).closest(".form-segment").removeClass("has-value");
        $(this).next(".placeholder__text").removeClass("is-hidden");
      }
    });

  $('input[type="radio"]').on("focus", function () {
    $(this).next(".radio__value").addClass("has-focus");
  });

  $('input[type="radio"]').on("focusout", function () {
    $(this).next(".radio__value").removeClass("has-focus");
  });

  $(".radio__label").click(function () {
    $(this).closest(".form-segment").addClass("has-value");
  });

  $("input[type=radio]").change(function () {
    if ($("#legalYes").is(":checked")) {
      $(".js-legal-yes").removeClass("is-hidden");
      $("#legal_detail").focus();
    } else {
      $(".js-legal-yes").addClass("is-hidden");
    }

    if ($("#dependantsYes").is(":checked")) {
      $(".js-dependants-detail").removeClass("is-hidden");
      $("#dependants_detail").focus();
    } else {
      $(".js-dependants-detail").addClass("is-hidden");
    }

    if ($("#providerYes").is(":checked")) {
      $(".js-provider-detail").removeClass("is-hidden");
      $("#provider_detail").focus();
    } else {
      $(".js-provider-detail").addClass("is-hidden");
    }

    if ($("#pregnantYes").is(":checked")) {
      $(".js-seeking-maternity-care").removeClass("is-hidden");
    } else {
      $(".js-seeking-maternity-care").addClass("is-hidden");
    }

    if ($("#historyBirthYes").is(":checked")) {
      $(".js-birth-yes").removeClass("is-hidden");
      $("#birth_year1").focus();
    } else {
      $(".js-birth-yes").addClass("is-hidden");
    }

    if ($("#female").is(":checked")) {
      $(".js-pregnant").removeClass("is-hidden");
    } else {
      $(".js-pregnant").addClass("is-hidden");
    }

    if ($("#historyFemale").is(":checked")) {
      $(".js-female").removeClass("is-hidden");
    } else {
      $(".js-female").addClass("is-hidden");
    }

    if ($("#cigarettesYes").is(":checked")) {
      $(".js-cigarettes-amount").removeClass("is-hidden");
      $("#how_many_cigarettes").focus();
    } else {
      $(".js-cigarettes-amount").addClass("is-hidden");
    }

    if ($("#alcoholYes").is(":checked")) {
      $(".js-drinks-amount").removeClass("is-hidden");
      $("#how_many_drinks").focus();
    } else {
      $(".js-drinks-amount").addClass("is-hidden");
    }
  });

  $("input[name='how_many_family_members']").change(function () {
    if ($("input[name='how_many_family_members']:checked").val() > 0) {
      $(".js-family-count").removeClass("is-hidden");
      $("#dependants").focus();
    } else {
      $(".js-family-count").addClass("is-hidden");
    }
  });

  $('[data-trigger="modal"]').click(function () {
    $(this).next('[data-item="modal"]').addClass("is-visible");
    $("body")
      .addClass("is-locked")
      .find(".modal__overlay")
      .addClass("is-visible");
    $("body").css("padding-right", window.getScrollbarWidth());
  });

  $('[data-trigger="close"]').click(function () {
    $(this).closest('[data-item="modal"]').removeClass("is-visible");
    $("body")
      .removeClass("is-locked")
      .find(".modal__overlay")
      .removeClass("is-visible");
    $("body").removeAttr("style");
  });

  $(".modal__overlay").click(function () {
    console.log("overlay clicked");
    $(this).removeClass("is-visible");
    $("body")
      .removeClass("is-locked")
      .find('[data-item="modal"].is-visible')
      .removeClass("is-visible");
  });

  setTimeout(function () {
    $("#submit").prop("disabled", false);
  }, 5000);

  var happy = {
    USPhone: function (val) {
      return /^\(?(\d{3})\)?[\- ]?\d{3}[\- ]?\d{4}$/.test(val);
    },

    date: function (val) {
      return /^(?:0[1-9]|1[0-2])\/(?:0[1-9]|[12][0-9]|3[01])\/(?:\d{4})/.test(
        val
      );
    },

    number: function (val) {
      return /^[0-9]*$/.test(val);
    },

    link: function (val) {
      return /^((?!http).)*$/.test(val);
    },

    email: function (val) {
      return /^(?:\w+\.?\+?)*\w+@(?:\w+\.)+\w+$/.test(val);
    }
  };

  $("#register").isHappy({
    fields: {
      "#care_card": {
        required: true,
        message: "Care card number is required",
        test: happy.number
      },
      "#first_name": {
        required: true,
        message: "First name is required"
      },
      "#last_name": {
        required: true,
        message: "Last name is required"
      },
      "#last_name": {
        required: true,
        message: "Last name is required"
      },
      "#month_of_birth": {
        required: true,
        message: "Required"
      },
      "#day_of_birth": {
        required: true,
        message: "Required"
      },
      "#year_of_birth": {
        required: true,
        message: "Required"
      },
      "#occupation": {
        required: true,
        message: "Occupation is required"
      },
      "#street": {
        required: true,
        message: "Street Address is required"
      },
      "#city": {
        required: true,
        message: "City is required"
      },
      "#postal_code": {
        required: true,
        message: "Postal code is required"
      },
      "#email": {
        required: true,
        message: "A valid email address is required",
        test: happy.email
      },
      "#primary_phone": {
        required: true,
        message: "Primary phone is required"
      }
    },
    errorTarget: ".form__segment"
  });

  $("#history").isHappy({
    fields: {
      "#history_first_name": {
        required: true,
        message: "First name is required"
      },
      "#history_last_name": {
        required: true,
        message: "Last name is required"
      },
      "#history_email": {
        required: true,
        message: "A valid email address is required",
        test: happy.email
      },
      "#history_month_of_birth": {
        required: true,
        message: "Required"
      },
      "#history_day_of_birth": {
        required: true,
        message: "Required"
      },
      "#history_year_of_birth": {
        required: true,
        message: "Required"
      },
      "#current_height": {
        required: true,
        message: "Current height is required"
      },
      "#current_weight": {
        required: true,
        message: "Current weight is required"
      },
      "#send_medical_records": {
        required: true,
        message: "Field is required"
      }
    },
    errorTarget: ".form__segment"
  });

  $("#contact").isHappy({
    fields: {
      "#name": {
        required: true,
        message: "Name is required"
      },
      "#email": {
        required: true,
        message: "A valid email address is required",
        test: happy.email
      },
      "#phone_number": {
        required: true,
        message: "Phone number is required"
      },
      "#message": {
        message: "Your message must not contain links",
        test: happy.link
      }
    },
    errorTarget: ".form__segment"
  });

  // $("#flu-shot").isHappy({
  //   fields: {
  //     "#first_name": {
  //       required: true,
  //       message: "First name is required"
  //     },
  //     "#last_name": {
  //       required: true,
  //       message: "Last name is required"
  //     },
  //     "#email": {
  //       required: true,
  //       message: "A valid email address is required",
  //       test: happy.email
  //     },
  //     "#month_of_birth": {
  //       required: true,
  //       message: "Required"
  //     },
  //     "#day_of_birth": {
  //       required: true,
  //       message: "Required"
  //     },
  //     "#year_of_birth": {
  //       required: true,
  //       message: "Required"
  //     },
  //     "#phone_number": {
  //       required: true,
  //       message: "Phone number is required"
  //     },
  //     "#availability": {
  //       required: true,
  //       message: "Please let us know you preferred time and dates"
  //     }
  //   },
  //   errorTarget: ".form__segment"
  // });

  // With a custom message
  $("form").areYouSure({
    message:
      "Navigating away from this page will reset the form. Are you sure you want to do that?"
  });
})();

//uses classList, setAttribute, and querySelectorAll
//if you want this to work in IE8/9 youll need to polyfill these
(function () {
  var d = document,
    collapseToggles = d.querySelectorAll(".js-collapseTrigger"),
    setAria,
    setcollapseAria,
    switchcollapse,
    touchSupported = "ontouchstart" in window,
    pointerSupported = "pointerdown" in window;

  skipClickDelay = function (e) {
    e.preventDefault();
    e.target.click();
  };

  setAriaAttr = function (el, ariaType, newProperty) {
    el.setAttribute(ariaType, newProperty);
  };
  setcollapseAria = function (el1, el2, expanded) {
    switch (expanded) {
      case "true":
        setAriaAttr(el1, "aria-expanded", "true");
        setAriaAttr(el2, "aria-hidden", "false");
        break;
      case "false":
        setAriaAttr(el1, "aria-expanded", "false");
        setAriaAttr(el2, "aria-hidden", "true");
        break;
      default:
        break;
    }
  };
  //function
  switchcollapse = function (e) {
    console.log("triggered");
    e.preventDefault();
    var thisAnswer = e.target.parentNode.nextElementSibling;
    var thisQuestion = e.target;
    if (thisAnswer.classList.contains("is-collapsed")) {
      setcollapseAria(thisQuestion, thisAnswer, "true");
    } else {
      setcollapseAria(thisQuestion, thisAnswer, "false");
    }
    thisQuestion.classList.toggle("is-collapsed");
    thisQuestion.classList.toggle("is-expanded");
    thisAnswer.classList.toggle("is-collapsed");
    thisAnswer.classList.toggle("is-expanded");

    thisAnswer.classList.toggle("animateIn");
  };
  for (var i = 0, len = collapseToggles.length; i < len; i++) {
    if (touchSupported) {
      collapseToggles[i].addEventListener("touchstart", skipClickDelay, false);
    }
    if (pointerSupported) {
      collapseToggles[i].addEventListener("pointerdown", skipClickDelay, false);
    }
    collapseToggles[i].addEventListener("click", switchcollapse, false);
  }
})();

(function () {
  "use strict";
  var getScrollbarWidth, scrollbarWidth;

  scrollbarWidth = null;

  getScrollbarWidth = function (recalculate) {
    var div1, div2;
    if (recalculate == null) {
      recalculate = false;
    }
    if (scrollbarWidth != null && !recalculate) {
      return scrollbarWidth;
    }
    if (document.readyState === "loading") {
      return null;
    }
    div1 = document.createElement("div");
    div2 = document.createElement("div");
    div1.style.width = div2.style.width = div1.style.height = div2.style.height =
      "100px";
    div1.style.overflow = "scroll";
    div2.style.overflow = "hidden";
    document.body.appendChild(div1);
    document.body.appendChild(div2);
    scrollbarWidth = Math.abs(div1.scrollHeight - div2.scrollHeight);
    document.body.removeChild(div1);
    document.body.removeChild(div2);
    return scrollbarWidth;
  };

  if (typeof define === "function" && define.amd) {
    define([], function () {
      return getScrollbarWidth;
    });
  } else if (typeof exports !== "undefined") {
    module.exports = getScrollbarWidth;
  } else {
    this.getScrollbarWidth = getScrollbarWidth;
  }
}.call(this));
