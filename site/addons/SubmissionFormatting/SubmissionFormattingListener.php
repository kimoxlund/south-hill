<?php

namespace Statamic\Addons\SubmissionFormatting;

use Statamic\API\Path;
use Statamic\Extend\Listener;

class SubmissionFormattingListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.add_to_head' => 'initSubmissionFormatting',
    ];

    public function initSubmissionFormatting()
    {
        $addon_css = $this->css->url('submission-formatting.css');
        $addon_js = $this->js->url('submission-formatting.js');
        $html = '<link rel="stylesheet" href="' . $addon_css . '">';
        $html .= '<script src="' . $addon_js . '"></script>';
        return $html;
    }
}
