if ( ! window.deferAfterjQueryLoaded ) {
    window.deferAfterjQueryLoaded = [];
    Object.defineProperty(window, "$", {
        set: function(value) {
            window.setTimeout(function() {
                $.each(window.deferAfterjQueryLoaded, function(index, fn) {
                    fn();
                });
            }, 0);
            Object.defineProperty(window, "$", { value: value });
        },

        configurable: true
    });
}

window.deferAfterjQueryLoaded.push(function() {

    if(window.location.href.indexOf('submission') > -1) {

        $('table').each(function (){
            $(this).replaceWith( $(this).html()
                .replace(/<tbody/gi, "<div class='table'")
                .replace(/<tr/gi, "<div class='submission-field'")
                .replace(/<th>/gi, "")
                .replace(/<\/th>/gi, ":")
                .replace(/<td>/gi, "<span class='submission-value'>")
                .replace(/<\/td>/gi, "</span>")
                .replace(/<\/tr>/gi, "<\/div>")
                .replace(/<\/tbody/gi, "<\/div")
            );
        });
    }

    if(window.location.href.indexOf('history/submission') > -1) {

		var monthOfBirth = $( ".submission-value:eq( 4 )");
		var dayOfBirth = $( ".submission-value:eq( 5 )");
		var yearOfBirth = $( ".submission-value:eq( 6 )");

		var dateOfBirth = $.trim(monthOfBirth.text()) + ' ' + $.trim(dayOfBirth.text()) + ', ' + $.trim(yearOfBirth.text());

		$(yearOfBirth).parent().after("<div class='submission-field'>Date of birth: <span class='submission-value'>" + dateOfBirth + "</span></div>")

		removeEmptyFields();
    }

    if(window.location.href.indexOf('register/submission') > -1) {

		var monthOfBirth = $( ".submission-value:eq( 7 )");
		var dayOfBirth = $( ".submission-value:eq( 8 )");
		var yearOfBirth = $( ".submission-value:eq( 9 )");

		var dateOfBirth = $.trim(monthOfBirth.text()) + ' ' + $.trim(dayOfBirth.text()) + ', ' + $.trim(yearOfBirth.text());

		$(yearOfBirth).parent().after("<div class='submission-field'>Date of birth: <span class='submission-value'>" + dateOfBirth + "</span></div>")

    	removeEmptyFields();
    }


    function removeEmptyFields() {
    	$('.submission-value').each(function() {
			if ( $.trim( $(this).text() ).length == 0 ) {
				if ( $(this).children().length == 0 ) {
					$(this).parent().remove();
				}
			}
		});

		$(yearOfBirth).parent().remove();
		$(dayOfBirth).parent().remove();
		$(monthOfBirth).parent().remove();

		$('.submission-field').after('<br>');
    }

});