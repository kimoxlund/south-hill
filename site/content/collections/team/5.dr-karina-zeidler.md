---
title: Dr. Karina Zeidler
job_title: Family Doctor
headshot: /assets/img/team/Karina SH-7484.jpg
id: 2c29f19d-5970-465c-81c0-ba3d07edcbcb
---
<p>Karina is a graduate of the UBC Family Practice Program. In addition, she completed three months of extra training in women’s health. She grew up overseas, is a fluent French speaker, and has lived all across Canada. She has called Vancouver her home since 2002. Her special interests are in reproductive justice, transgender health, and prison medicine. Karina also loves general every day family practice and enjoys working with people to achieve their health goals in a way that fits in with their lives and values. Outside of work, Karina loves horsing around with her children, eating out, live music, and arguing about politics.
</p>