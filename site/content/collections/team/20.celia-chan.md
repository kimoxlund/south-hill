---
title: Celia Chan
job_title: Office Manager
headshot: /assets/img/team/celia-chan.jpg
id: b558a600-ee5c-4082-95b5-95437811bf0f
---
<p>All of our patients know or will come to know Celia! She is on the front lines and smoothly runs the day-to-day operations of our clinic. She has a Medical Office Assistant certificate and has worked in a variety of different clinical settings prior to joining our team. Outside of her work she likes to travel, read, spend time with her family, and photograph anything interesting she comes across. She likes working with our inter-professional team and enjoys the diverse patient population at South Hill. She also enjoys helping with the cute babies! As an added bonus for our patients- she also speaks Mandarin.
</p>