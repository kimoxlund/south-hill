---
title: Chelsea Reed
job_title: 'Digital Communications & Intake Coordinator'
headshot: /assets/img/team/Chelsea SCBP-7316.jpg
id: e0b9c620-9850-4232-8c27-282effbd25ff
---
<p>Chelsea grew up in the Okanagan and now resides in beautiful Victoria, BC where she works remotely for the clinic. She holds a diploma in Community Health Care, Public Relations and is a Certified Child Passenger Safety Tech. After switching careers into maternity from nearly a decade of working in geriatric care, she discovered her love for helping families and all things maternity! Currently she is studying to become a Baby Planner and plans to launch her own company to help expecting parents prepare for the arrival of their baby. Chelsea enjoys reading, attending trade-shows and conferences, walking along the ocean and planning baby showers. Her favourite part of working at South is helping clients feel welcomed to the clinic.
</p>