---
title: ' Dr. Maralyn Hope'
job_title: Family Doctor
headshot: /assets/img/team/maralyn-hope.jpg
id: b656d3ae-4f40-402f-b589-9ecb349ac018
---
<p>Maralyn is a graduate of UBC Medical School and the Vancouver Fraser Family Medicine residency program. She was born and raised in Vancouver and loves the west coast. She chose family practice because of the longitudinal relationships formed between physician and patients. She also loves the wide scope of practice that a family physician is exposed to. She loves working at the South Hill Health Centre as it’s a fresh and innovative approach to health care.
</p>
<p>Outside of work, Maralyn enjoys staying active with her husband and daughter. Maralyn had her second child, a son, in 2016.
</p>
<p>Lauren is now expecting her third baby and will be on maternity leave in 2019.<br>
</p>