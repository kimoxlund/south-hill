---
title: COVID-19 PATIENT UPDATES
is_hidden: false
masthead_image: /assets/img/background/mural.jpg
template: page
fieldset: page
id: e3eb3d1a-dd0a-4427-990d-0afa1fed5447
---

<p><strong></strong>
</p>
<p><strong>COVID-19 Self-Assessment Tool here: <a href="https://covid19.thrive.health/">https://covid19.thrive.health/</a> </strong>
</p>
<p><strong>If your employer has requested a sick note in order for you to be off work related to COVID19 illness, or risk of illness, please provide them with the following letter. You<em> do not need </em><em>a medical certificate from your doctor, NP or midwife:</em> <a href="https://bcfamilydocs.ca/wp-content/uploads/2020/03/COVID-19-Employer-Sick-Note-BC-Family-Doctors-25032020.pdf">https://bcfamilydocs.ca/wp-content/uploads/2020/03/COVID-19-Employer-Sick-Note-BC-Family-Doctors-25032020.pdf</a></strong>
</p>
<p><strong></strong><br>
</p>
<table>
<tbody>
<tr>
	<td>
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
	</td>
</tr>
</tbody>
</table>
<p><img src="https://lh6.googleusercontent.com/8pAJuIllJ1jzerKL8buqRREYwJDptm_46QwUs9Tz1rgwwq0HsePTb7q4VoFBYsV4tO6vg3GCauNGxFA7Clu9NFafMgf9RsQVDwOi2OqcVqeoPPZ3TsC51pbavmkzXOKKp9XWhqso">
</p>
<p><strong>200 – 1193 Kingsway, Vancouver, BC, V5V 3C9  •  Office: 604 428 8878  •  Fax: 604 428 8871</strong>
</p>
<p><strong>STUDENTS AND BACK TO SCHOOL </strong>
</p>
<p>To Whom It May Concern,
</p>
<p>This child is under the care of one of a Nurse Practitioner (NP) or Family Physician (FP) at South Hill Family Health Centre.
</p>
<p>Below are the following situations in which it is safe for a student to return to school or child care after being ill, or being exposed to someone with an illness. Physician notes are not required for any of these situations. In fact, they are advised against to prevent our health care system from becoming overwhelmed.
</p>
<ul>
	<li>If symptoms are consistent with a previously diagnosed health condition that is not unusual for the child, they may return to school or child care without a COVID19 swab, or assessment by a FP/NP. <i>(Example: runny nose/sneezing from previously diagnosed seasonal allergies)</i></li>
	<li>If a child had mild symptoms that did not include a fever and these symptoms have resolved within 24h of onset, the child can return to school or child care without a COVID19 swab, or assessment by a FP/NP. (<i>Example: fatigue, headache, runny nose, nausea)</i></li>
	<li>If a child has symptoms of fever, chills, cough, shortness of breath, vomiting, diarrhea, or loss of sense of smell or taste, they should have a COVID19 swab. If the swab result is negative for COVID19, they can return to school or child care when they are feeling well enough. Their symptoms do NOT need to have completely resolved.
	<ul>
		<li>If the child is not tested for COVID19, they should stay home for 10 days after symptom onset and can return if feeling well enough after 10 days</li>
	</ul></li>
	<li>If a child is asymptomatic, but their household contact has symptoms of illness and has tested negative for COVID19, or the test is pending, the child can continue in school or child care without a COVID19 swab, or assessment by a FP/NP. </li>
</ul>
<p>(<i>Example: Child had fever and cough and COVID19 swab negative. This child is feeling well 6 days after symptom onset but still has a mild dry cough; they are ok to return to school or child care)</i>
</p>
<p>These recommendations are in accordance with the most recent guidelines from the BC Centre for Disease Control (Sept 11, 2020; <a href="http://www.bccdc.ca/Health-Info-Site/Documents/COVID_%20public_guidance/Guidance-k-12-schools.pdf">http://www.bccdc.ca/Health-Info-Site/Documents/COVID_ public_guidance/Guidance-k-12-schools.pdf</a>;
</p>
<p>(Sept 25, 2020 http://www.bccdc.ca/Health-Info-Site/Documents/COVID_public_guidance/Guidance_Child_Care.pdf).
</p>
<p>Guidelines are expected to continue to evolve throughout the school year, and South Hill will update this letter accordingly.
</p>
<p>Please do not hesitate to contact South Hill Family Health Centre with concerns. We greatly appreciate the role of all educators and school employees in supporting a safe return to school for children.
</p>
<p>Sincerely,
</p>
<p>Dr. Stephanie Stacey (MDCM, MSc, BSc) on behalf of the South Hill NPs/FPs
</p>
<p>South Hill Family Health Centre
</p>
<p><img src="https://ci3.googleusercontent.com/proxy/zmlhdWCwrtSHt5tWTitQDTDWcFU7aQzxcG4_rGfkPlzhtj_Gld1igeZV53NUg7PZw2c0mnpi4wbpaCyqaSdvN73QROoYCVHsIkCXv8r4z0CEi9FHGfu-SiO1GQvMJWRIkjvGrTqLuUWfRpH58EKJbjY2QktSqQ=s0-d-e1-ft#https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/images/16fd38b9-bee6-4918-b331-441ba7289a60.png"><br>
</p>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<h2><strong>The SOUTH Update<br>COVID-19: October 2020</strong></h2>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table class="m_7160148765403798431mcnDividerBlock">
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><em><strong>Be calm, be kind, be safe. </strong></em>
				</p>
				<p>~Dr. Bonnie Henry
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table class="m_7160148765403798431mcnDividerBlock">
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><strong>* New Content</strong>
				</p>
				<ol>
					<li><em><strong>****Flu Shot Clinics at South Hill - Oct 13-30 - book your family appt now!****</strong></em></li>
					<li>*Public Health Flu Shot Clinics - Starting Nov 3 - see below for where and when* </li>
					<li>*Anti-Racism committee update</li>
					<li>Waiting room and wearing masks</li>
					<li>South Hill - <strong>Most appointments STILL via phone/video </strong></li>
					<li>Vaccine clinics: ***<em>new dates </em></li>
					<li>Baby care basics: ***<em>new dates</em></li>
					<li>Motherlode - Pathways forward for new mothers : ***<em>new dates </em></li>
					<li>SCBP - Prenatal Care: ***<em>new update </em></li>
					<li>SCBP - Connecting Pregnancy Zoom groups</li>
					<li>SCBP - Postpartum Care & Zoom Drop-in: ***<em>new update </em></li>
					<li>Registered Clinical Counselor sessions - <em>virtual, in clinic and outside </em></li>
					<li>*Return to school information</li>
					<li>* COVID-19 Dashboard for BC</li>
				</ol>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table class="m_7160148765403798431mcnDividerBlock">
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>1. </strong><strong>Flu Clinics at South Hill - October 13 to 30</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>A limited number of flu vaccines will be available for our patients starting <strong><em>Tuesday, October 13th to Friday, October 30</em></strong>. Priority is given to families with children from 6 months to 5 years of age. The family will all come together for their flu shots. Children under the age of 9 who have never had a flu vaccine, need 2 doses, 4 weeks apart, for sufficient protection. We are also prioritizing seniors 65 years of age or older and patients at risk with chronic health issues.
				</p>
				<p>The Flu Shot clinics will be held:
				</p>
				<p><em><u><strong>Monday to Friday  from  9am-12pm AND 2pm-5pm</strong></u></em>
				</p>
				<p><strong><em>You must email the clinic to book an appointment. Please do not phone to book a flu shot appointment. </em></strong>
				</p>
				<p>Go to the <a href="http://www.southhillhealth.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.southhillhealth.com&source=gmail&ust=1603482362049000&usg=AFQjCNET2VWFKnj9Qti9hdJYxKp06TGQ-g">southhillhealth.com</a> website to book an appointment or <a href="https://www.southhillhealth.com/flu-shot-clinics" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://www.southhillhealth.com/flu-shot-clinics&source=gmail&ust=1603482362049000&usg=AFQjCNEiJJUNToiKvLV30DlszI4t0Ftrpg">click here</a>
				</p>
				<p>Specify if you want to come between 9am and 12pm  <strong>OR</strong>  2pm and 5pm.<strong>You cannot pick your exact time</strong>. Your appointment will be confirmed by email.
				</p>
				<p>We will email you back with a specific time and instructions for your appointment.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table class="m_7160148765403798431mcnDividerBlock">
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>2. </strong><strong>Public Health Flu Shot Clinics</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td><br>If you are looking for more options to receive your flu vaccine, click the button below for a list of community flu clinics within Vancouver.<br>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="Click for Vancouver Coastal Health Community Flu Clinics" href="https://vancouverfluclinic.janeapp.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://vancouverfluclinic.janeapp.com/&source=gmail&ust=1603482362049000&usg=AFQjCNFl3y95hjedKU0cxeSzXMdmnwxD7Q">Click for Vancouver Coastal Health Community Flu Clinics</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>3. Anti-Racism Committee Update</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>The <em>South Clinic Anti-Racism Committee</em> continues to meet to analyze our current practices and develop a plan for an actively anti-racist and anti-oppressive clinic.
				</p>
				<p>We are currently in the process of organizing anti-racism trainings for all South care providers and doulas through the <a href="https://www.theheartcollaborative.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://www.theheartcollaborative.com/&source=gmail&ust=1603482362049000&usg=AFQjCNFOpHV-poMn0FAHs7arn93kxDaUpQ">Health Equity and Anti-Racism Training (HEART) Collaborative</a> and the <a href="https://www.nestingdoulacollective.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://www.nestingdoulacollective.com/&source=gmail&ust=1603482362049000&usg=AFQjCNHzuWxmw3dB211wGiFREsOWzx6vRA">Nesting Doula Collective</a>.
				</p>
				<p>On September 30th, the South team wore orange for <a href="https://www.orangeshirtday.org/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://www.orangeshirtday.org/&source=gmail&ust=1603482362049000&usg=AFQjCNHcIPzdU0QD6LFt1xkZSOtOxHgu7Q">Orange Shirt Day</a> to honour Indigenous communities and survivors of the residential school system. Health disparities between Indigenous and non-Indigenous people are rooted in colonialism. We know that racism is a public health crisis and we support the <a href="https://engage.gov.bc.ca/addressingracism/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://engage.gov.bc.ca/addressingracism/&source=gmail&ust=1603482362049000&usg=AFQjCNGuyy0deSv1jBqdoC8OLSMNvDld6A">upcoming independent inquiry</a> into racism in BC’s healthcare system.
				</p>
				<p>High-quality data and research are necessary to properly address health disparities and promote system-level change. Please consider participating in the <a href="https://respcct.ca/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://respcct.ca/&source=gmail&ust=1603482362049000&usg=AFQjCNGsORxPO9l7X-1O0ZV5GV8pLUGH1Q">Research Examining the Stories of Pregnancy and Childbearing in Canada Today (RESPCCT) study</a>, which is addressing research gaps by examining people’s experiences of pregnancy and birth across diverse cultures, backgrounds, circumstances, and identities.
				</p>
				<p>We’ll provide another update on our activities next month. In the meantime, please take a look at our updated <a href="https://docs.google.com/document/d/11hZmK3ZncSLmsicQ88-iLLmdrSwmbvWrVRnQnvR4eV4/edit?usp=sharing" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://docs.google.com/document/d/11hZmK3ZncSLmsicQ88-iLLmdrSwmbvWrVRnQnvR4eV4/edit?usp%3Dsharing&source=gmail&ust=1603482362049000&usg=AFQjCNHSsU-o4YTs_FCJQyE9YX9t28K9OA">South Clinic Anti-Racism Resource List </a>. Please don’t hesitate to let us know if you have any additions - as always, we welcome feedback and suggestions about this work.
				</p>
				<p><em>On behalf of the South Clinic Anti-Racism Committee</em>
				</p>
				<p><em>Heba Al-Nashef, RM; Preveena Dharmaraj, MD: Rebecca Dickinson, RM; Kiran Nayar, MD; Tobi Reid, RM</em>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>4. Waiting room and wearing masks</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>With flu-season approaching we remain ever vigilant on physical distancing. You will see all our staff wearing masks.
				</p>
				<p><em><strong>Masks are mandatory for everyone during visits. We ask that all patients, over the age of 5, wear masks. </strong></em>
				</p>
				<p>Please bring your own mask as we have very limited supply. If you forget your mask, you can purchase one from us for $2.00.
				</p>
				<p>Please DO NOT come up to the clinic early for your appointment. In order to maintain physical distancing, we must keep as few people in the clinic as possible. This may mean that we ask you to wait outside after you have checked in.  <strong>Likewise please leave the clinic immediately after your appointment.  Please call or email us to book a follow up appointment or request any records.</strong> Thanks for your cooperation to keep us all safe.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="Click for info about mask wearing" href="http://www.bccdc.ca/health-info/diseases-conditions/covid-19/prevention-risks/masks" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.bccdc.ca/health-info/diseases-conditions/covid-19/prevention-risks/masks&source=gmail&ust=1603482362049000&usg=AFQjCNHKqNt7klSmh5Wu6ci0CnRaQbJibA">Click for info about mask wearing</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>5. </strong><strong>South Hill - Most appointments STILL via phone/video </strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>**PLEASE READ BEFORE YOUR NEXT APPOINTMENT **
				</p>
				<p><strong>In-clinic visits remain extremely restricted</strong> as the providers are often working outside the clinic. <em>All in-person appointments at the clinic must be booked a doctor or NP  and can not be booked online.</em> If you feel you need to be seen in person, you must first have a phone appointment and the doctor/NP will help you from there.
				</p>
				<p>We have noticed an increasing number of “no answer” type “no shows” for scheduled phone/video appointments. As our days are often fully booked, we may have to move on to the next patient if you are not available when we call. In order to make sure we can provide appointments to everyone who needs them, please:
				</p>
				<p>**Make sure you provide THE BEST PHONE NUMBER for you to be reached at, when you book your appointment online.
				</p>
				<p>**Be ready to receive the call from 5 minutes before to 30 minutes after your booked time.
				</p>
				<p>Being ready means:
				</p>
				<ul>
					<li>call blocker shut off on your phone (so you can accept a call from a blocked number)</li>
					<li>sitting in a quiet, private space, other distractions minimized</li>
				</ul>
				<p>**NOTE: we are not able to do an appointment while you are driving, or otherwise committed to another task that requires your full attention.
				</p>
				<p>**If you need to cancel or reschedule your appointment, please use the same process you would for an in person appointment by emailing us @ <a href="mailto:south.families@gmail.com" target="_blank">south.families@gmail.com</a> or leaving a voicemail message with at least 24 hours notice.
				</p>
				<p>**Missed appointments may incur a missed appointment charge that you will need to pay before your next appointment**
				</p>
				<p>If you're booking an in-person appointment with a concern about your skin, it can be really helpful to your provider to send a photo of the skin concern at least 1 day ahead of your appointment to <a href="mailto:south.families@gmail.com" target="_blank">south.families@gmail.com</a>. <br><br>Tips for taking a good photo:<br>1. Use good light, if blurry, please take again.<br>2. If it is something small, it can be helpful to place a dime beside it in the photo for us to be able to tell size.
				</p>
				<p><strong><em>**Masks are mandatory at all times while you are in the clinic**</em></strong>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="Click here for details on booking virtual appointments" href="https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/af0d1633-e106-4c4e-a3af-3e2ec8feaf1b/South_Hill_Virtual_Appointments_March_28.pdf" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/af0d1633-e106-4c4e-a3af-3e2ec8feaf1b/South_Hill_Virtual_Appointments_March_28.pdf&source=gmail&ust=1603482362050000&usg=AFQjCNHMS-mMP0qdRVesrAomPsALFAKSyQ">Click here for details on booking virtual appointments</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td>
						<p><strong>We will alert you to when we will be widely opening our doors to see patients. For now, we are continuing with restricted in-person clinic appointments. We are following the Doctors of BC Guidelines for Expanding in-person care. You can find this guideline here.</strong><br><br><em><strong><a href="https://bcfamilydocs.ca/recommendations-for-expanding-in-person-care" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://bcfamilydocs.ca/recommendations-for-expanding-in-person-care&source=gmail&ust=1603482362050000&usg=AFQjCNE8qinAHTUOhIFlzmib7reXLaeFrg">https://bcfamilydocs.ca/<wbr>recommendations-for-expanding-<wbr>in-person-care</a></strong></em>
						</p>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>6. </strong><strong>Vaccine Clinics: INFANT and CHILD ROUTINE VACCINES</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><em>If appropriate with you and your care provider, you may choose to have vaccines with your regional PUBLIC HEALTH OFFICE. Contact them directly for scheduling information.</em>
				</p>
				<p><em><strong>We are now booking vaccines for children age 4 to 6 years, as well as babies. </strong></em>
				</p>
				<p>To minimize your interaction with other families and staff at the clinic we have specific clinics for spaced out,<strong><em> vaccine only</em></strong> appointments.<strong> We will not be offering flu vaccines at these appointments.</strong>
				</p>
				<p>Vaccine clinic dates:  We are running vaccine clinics on specific days throughout October so book your phone or video visit to discuss your baby's growth and development and at that appointment the providers will book your child’s vaccine appointment at the next available clinic.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="Click for details about booking a Vaccine Appointment" href="https://docs.google.com/document/d/1hslUIAgF1CS2ipJovqTcuYPgXwkNfOPg2iRC08VFs6s/edit?usp=sharing" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://docs.google.com/document/d/1hslUIAgF1CS2ipJovqTcuYPgXwkNfOPg2iRC08VFs6s/edit?usp%3Dsharing&source=gmail&ust=1603482362050000&usg=AFQjCNFWsbseZw7Lpb4QmpPaa8AdTdX8aQ">Click for details about booking a Vaccine Appointment</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>7. Baby Care Basics Workshops - <em>New dates</em></strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td>
						<h4><em><strong>Baby Care Basics - 1 to 6 months</strong></em></h4>
						<p><br>This virtual session welcomes families with babies between the ages of 1 to 6 months for an informal and collaborative educational session about baby care including; baby (and parent) sleeping, introducing solids, tender baby skin care and safety in the home.  <br><br>Pre-registration is required and is limited to 15 families/babies per session. To register please please call South Hill Clinic (604-428-8878) or send an email with your name and the Subject Line: Baby Basics to: <a href="mailto:south.families@gmail.com" target="_blank">south.families@gmail.com</a>.<br><br>The next session will be held on: <br><em><strong> Friday, October 30th:  3:00 - 4:30 PM  - FREE**</strong></em><br><strong>Friday, November 13th: 3:00 - 4:30 PM - FREE**</strong>
						</p>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td>
						<h4><em><strong>Baby Care Basics - 6 months to 1 year</strong></em></h4>
						<p>This virtual session welcomes families with babies between the ages of 6 months to 1 year for an informal and collaborative educational session about baby care including;  sleep and safety tips for the child on the move; review of solids and food progression, breastfeeding & weaning, baby’s developmental milestones.  <br><br>Pre-registration is required and is limited to 15 families/babies per session. To register please please call South Hill Clinic (604-428-8878) or send an email with your name and the Subject Line: Baby Basics to: <a href="mailto:south.families@gmail.com" target="_blank">south.families@gmail.com</a>.<br><br>The next session will be held on:
						</p>
						<p><em><strong>Thursday, October 22nd:  2:00 - 3:30 PM  - FREE**<br>Thursday, November 5th:  2:00 - 3:30 PM  - FREE**</strong></em>
						</p>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>This group is facilitated via Zoom (secure video) by<em> Dr. Maralyn Hope</em>. All patients at South Hill and SCBP are welcome to attend.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td>
						<h4><em><strong>8. Motherlode: Pathways Forward for New Mothers - New dates</strong></em></h4>
						<p><em><strong>This group is for those who identify as a mother</strong></em> who are struggling with any form of depression, anxiety or being overwhelmed in the postpartum period (baby or toddler up to 3 years of age).  The group runs a week for 6 weeks and covers information on diagnosis, symptoms and treatments of depression, anxiety & just simply feeling overwhelmed.  Self-care is an important focus that is woven into the 6 week course with specific sessions on nutrition, exercise, sleep, time for yourself and gathering support. We review goal setting together each week, specific to your self-care and well-being. This group runs for 6 weeks starting:
						</p>
						<p><em><strong>Wednesdays from November 18th to December 16th -  FREE**<br>2:00pm to  3:30 pm, virtually, on Zoom. </strong></em>
						</p>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>For the continuity and cohesiveness of the group, it is important that you commit to attending all six sessions. This is not a drop-in group. <br><br>Participation is limited and fills up quickly! If you are interested but are not accepted into this group, we will add your name to a waitlist for the next session. These groups are run ongoing throughout the year. <br><br>If you are interested, please send an email with your name, preferred email address, and the names and ages of your children to <a href="mailto:south.families@gmail.com" target="_blank">south.families@gmail.com</a>. In order to be eligible, you need to be able to converse and read English.<br><br>This group is facilitated via Zoom (secure video) by <em>Dr. Maralyn Hope</em>. All patients at South Hill and SCBP are welcome to attend.  All patients at  South Hill and SCBP are welcome to attend.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>9.  South Community Birth Program - Prenatal Care</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>For <strong>Prenatal Care</strong>, First visit  - up to 12 wks -this will be done by phone or video. All relevant testing will be ordered.  We will see you for in person visits at approximately: 16 wks; 22 wks; 28 wks; 32 wks; 35 wks; 37 wks; 39 wks; and 40 wks (then weekly or more frequent as needed).<br>
				</p>
				<p>Due to a COVID surge and in order to maintain your safety in the clinic <strong><em>we are once again limiting patient time in the clinic.</em></strong> Flu season is around the corner and all patients in labour, with any signs or symptoms of COVID, are being treated as potentially having COVID and will be under investigation. For this reason we also want to limit your exposure to the seasonal flu and the common cold. <em><strong>Keeping face-to-face encounters less than 15 min also helps to reduce transmission. </strong></em><br>
				</p>
				<p><em>For visits in person, in the clinic, please arrive at your appointment time and stay in your car. Your care provider will call you and conduct the majority of your appointment on the phone/video and then ask you to come directly into a room for the physical exam portion of your visit. We will be cleaning the room before you enter. This is being done to minimize the amount of time you are in the clinic.  </em>
				</p>
				<p><strong><em>We ask your partner or support person to NOT come up and to please wait in the car. We are happy to do the physical exam while your chosen partner is on the phone or on video chat. </em></strong>
				</p>
				<p><strong><em>**Masks are mandatory at all times while you are in the clinic**</em></strong>
				</p>
				<p><br>All necessary lab requisitions and relevant educational materials will be emailed to you.  Your care provider will be booking your follow-up appointment details after every appointment. Some pregnant people may need fewer in-person visits, while others with complications will require more.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>10. South Community Birth Program - Connecting Pregnancy Zoom Groups</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>After some initial technical learning curve/glitches, our virtual zoom CP Groups are going well. A few pointers:
				</p>
				<ol>
					<li>You will receive an email with the meeting invite within 24-48 hours before your group date. Please read through the email and pdf documents and bring your signed consent forms with you to your belly check. Zoom meetings will not be recorded due to confidentiality. </li>
					<li>CP Zoom groups will mostly be held earlier in the afternoon and you will still be coming in for your regular belly check in the evening with your specified pod time time. You will be in and out of the clinic in 10 minutes for your belly check. <strong>Partners are asked to attend the visit by phone or video.</strong></li>
					<li>Please continue to maintain physical distancing in our clinic.  <strong><em>Please do not group together or stand in the hallways. </em></strong></li>
				</ol>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>11. SCBP - Postpartum Care & Zoom Drop-in - New dates</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p>For Postpartum Care, our hospital visits are done on day 1 and/or 2.  We are continuing  with a modified schedule for postpartum care, individually tailored to your needs.  <strong>Once you are 2 weeks postpartum, partners are asked to attend the visit by phone or video.</strong><br><br>All of your postpartum care takes place in our Connecting West room. We have created three offices for our nurses and have moved all PP resources into this one space. Seating is arranged to create social distancing. We strive to keep our clinic green - please use only one receiving blanket to change and weigh your baby then deposit into the laundry basket.
				</p>
				<ol>
					<li>We know how hard it is to get out of the house with a new baby! But please arrive a minimum of 20 minutes before your scheduled 45 minute appointment time.<em> If you are late, your 45 minute appointment will need to be shortened. If you are more than 20 minutes late, we will need to reschedule you.</em></li>
					<li>Please check in with admin staff  when you arrive. They will direct you to the PP waiting room in Connecting West. <em><strong>DO NOT SIT IN THE MAIN WAITING ROOM AREA. </strong></em> </li>
					<li>We know how difficult it is to get to the clinic after you have just given birth. For this reason partners or support people are welcome to attend the PP for the early visits. Talk to your nurse about your schedule of visits going forward and when it is possible to come on your own. Please be aware that we MUST continue to maintain physical distancing -<em> please do not group together or stand move from your socially distanced chair to admire another baby close up!</em></li>
					<li>Visits will be alternating with virtual and in-person clinics visits. Your nurse will plan these appointments with you. The schedule, approximately is:</li>
				</ol>
				<p><strong>First baby:</strong>
				</p>
				<p>&gt;In hospital or clinic: day 1 and 3
				</p>
				<p>&gt;Video or phone call: day 5
				</p>
				<p>&gt; Video or phone call: day 8 or 9
				</p>
				<p>&gt;In clinic: 2wk
				</p>
				<p>&gt; Video or phone call: at 4wk
				</p>
				<p>&gt;In clinic: discharge at 6 wks
				</p>
				<p><strong>Second or more baby:</strong>
				</p>
				<p>&gt;In hospital or clinic: day 1 and 3
				</p>
				<p>&gt;Video or phone call: day 5
				</p>
				<p>&gt; Video or phone call: day 8 or 9
				</p>
				<p>&gt; Video or phone call: day 14 or 15
				</p>
				<p>&gt;In clinic: at 3wk
				</p>
				<p>&gt;In clinic: discharge at 6 wks
				</p>
				<p><em><strong>Postpartum Drop-in Zoom Groups - Every Wednesday from 12pm to 2:00pm</strong></em>
				</p>
				<p><em>THESE DROP-IN ZOOM GROUPS ARE NOW OPEN TO EVERYONE - TELL YOUR FRIENDS  AND OTHER NEW PARENTS THEY ARE WELCOME TO JOIN US!</em>
				</p>
				<p><br>Postpartum Drop-in Group is now on Zoom! We continue to meet every Wednesday between 12-2. Guest speakers will be there to teach you something new, answer your questions and, along with Alison & Chelsea, help facilitate the sharing of your own knowledge. Check out the schedule on the SCBP website under “Workshops” where you will also find the link to the zoom meeting. Spread the word to your CP groupies & postpartum friends in general!<br>
				</p>
				<p><strong>October 14<sup>th</sup> </strong>Infant Massage (Doula Extraordinaire Noriko Ishibashi)<strong> </strong>
				</p>
				<p><strong>October 21<sup>st</sup> </strong>Navigating Mood Postpartum (Anna from Pacific Postpartum Support Society)
				</p>
				<p><strong>October 28<sup>th</sup> </strong>Topic TBD (Chiropractor Natasha Smith, DC)
				</p>
				<p><em>***Please check our website for all the Wednesday PP groups***</em>
				</p>
				<p><a href="https://scbp.ca/workshop/postpartum-drop-in-group" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://scbp.ca/workshop/postpartum-drop-in-group&source=gmail&ust=1603482362051000&usg=AFQjCNHLekyZ6y6I1CeLYjZf6M_0zNofSw"><em></em></a><em><a href="https://scbp.ca/workshop/">https://scbp.ca/workshop/</a><wbr>postpartum-drop-in-group</em>
				</p>
				<p><em> If there are any concerns about you or your baby’s well-being, at any time, you will be instructed to call the urgent on-call line at 604-875-2161 (ask for SCBP provider on-call)</em>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>12. Registered Clinical Counselor</strong><strong> </strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><br><em><strong>Danielle Schroeder  MA, RCC</strong></em><br>
						<p>Danielle is continuing to provide virtual appointments and also meeting with patients outside weather permitting or in the clinic, acknowledging that human connection, in person, can also be therapeutic. She has virtual or in-person appointments available on: <br><br><strong>Tuesdays between 9am and 5pm <br>Thursdays between 3pm and 8pm <br>Fridays between 9:30am and 5:30pm </strong>
						</p>
						<p>Other days and times can be made by special arrangement.
						</p>
						<p>For more information or to register please email Danielle at:
						</p><a href="mailto:yellaschroeder@gmail.com" title="yellaschroeder@gmail.com" target="_blank"><strong>yellaschroeder@gmail.com</strong></a>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>13. </strong><strong> Return to school information</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td><em><strong>Letter from South Hill - listing the situations in which it is safe for a student to return to school or child care after being ill, or being exposed to someone with an illness. Physician notes are not required for any of these situations. In fact, they are advised against to prevent our health care system from becoming overwhelmed. Download here:</strong></em>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="South Hill Return to School Letter " href="https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/58627271-b8d6-44ba-8637-00277b95dec3/Back_to_School_Letter_PDF_South_Hill_.pdf" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/58627271-b8d6-44ba-8637-00277b95dec3/Back_to_School_Letter_PDF_South_Hill_.pdf&source=gmail&ust=1603482362051000&usg=AFQjCNFSYa6enoYZnaz5K8JILRCBjNQAKg">South Hill Return to School Letter</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td><strong><em>Vancouver School Board - Daily health assessment</em></strong> <br>The VSB has sent a form to all parents to conduct a daily health assessment of your children before sending them to school.
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table>
		<tbody>
		<tr>
			<td><a class="m_7160148765403798431mcnButton" title="Click for daily health assessment example" href="https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/43878133-8248-412a-ad31-147e3b81df8b/Daily_Health_Assessment_Student_and_Parent_Copy.pdf" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://mcusercontent.com/2ac8a200c8b1245507ad8cffa/files/43878133-8248-412a-ad31-147e3b81df8b/Daily_Health_Assessment_Student_and_Parent_Copy.pdf&source=gmail&ust=1603482362051000&usg=AFQjCNHAROcjAsINP1cktd7H2AgOH_Ufbg">Click for daily health assessment example</a>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><em>These recommendations are in accordance with the most recent guidelines from the BC Centre for Disease Control:</em>
				</p>
				<p><a href="http://www.bccdc.ca/search?k=guidance-k-12-schools.pdf" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.bccdc.ca/search?k%3Dguidance-k-12-schools.pdf&source=gmail&ust=1603482362051000&usg=AFQjCNGPXJYAoBNWne23wVOwTOtQMdOTcw">http://www.bccdc.ca/search?k=<wbr>guidance-k-12-schools.pdf</a>
				</p>
				<p><a href="http://www.bccdc.ca/Health-Info-Site/Documents/COVID_public_guidance/Guidance_Child_Care.pdf" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.bccdc.ca/Health-Info-Site/Documents/COVID_public_guidance/Guidance_Child_Care.pdf&source=gmail&ust=1603482362051000&usg=AFQjCNFPCbxX-hsrUE7eAp947vSwpaccGA">http://www.bccdc.ca/Health-<wbr>Info-Site/Documents/COVID_<wbr>public_guidance/Guidance_<wbr>Child_Care.pdf</a>
				</p>
				<p>Guidelines are expected to continue to evolve throughout the school year, and South Hill will update this letter accordingly. Please do not hesitate to contact South Hill Family Health Centre with concerns. We greatly appreciate the role of all educators and school employees in supporting a safe return to school for children.
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><strong><em>Setting children & youth up for a successful return to school in the era of COVID-19:</em></strong>
				</p>
				<p>This was an excellent webinar on August 31. We attach the slides from this presentation here:
				</p>
				<p><em><a href="https://www2.gov.bc.ca/gov/content/education-training/k-12/covid-19-return-to-school" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://www2.gov.bc.ca/gov/content/education-training/k-12/covid-19-return-to-school&source=gmail&ust=1603482362051000&usg=AFQjCNHeo1EaDZjkRp0Ma3SF8jEmH8HbEg">https://www2.gov.bc.ca/gov/<wbr>content/education-training/k-<wbr>12/covid-19-return-to-school</a></em>
				</p>
				<p><em><strong>BC Children’s Hospital Paediatric Infectious Diseases Specialist Dr. Laura Sauvé talks about preparing children for back-to-school during the COVID-19 pandemic</strong></em>
				</p>
				<p><em><a href="http://www.bcchildrens.ca/about/news-stories/stories/preparing-children-for-back-to-school-during-the-covid19-pandemic" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.bcchildrens.ca/about/news-stories/stories/preparing-children-for-back-to-school-during-the-covid19-pandemic&source=gmail&ust=1603482362051000&usg=AFQjCNHVl8cmZwmdMTWu_M78_x-wSITf7A">http://www.bcchildrens.ca/<wbr>about/news-stories/stories/<wbr>preparing-children-for-back-<wbr>to-school-during-the-covid19-<wbr>pandemic</a></em>
				</p>
				<p><br><em><strong>COVID-19 and Children (ages 0-12) </strong></em>
				</p>
				<p>This is an excellent resource sheet prepared by the BC Centre for Disease Control. You will find information on how COVID-19 affects children and how to keep them safe and healthy, both mentally and physically.
				</p>
				<p><em><a href="http://www.bccdc.ca/health-info/diseases-conditions/covid-19/covid-19-and-children" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.bccdc.ca/health-info/diseases-conditions/covid-19/covid-19-and-children&source=gmail&ust=1603482362051000&usg=AFQjCNEd3u1PUiBSIYRwQj-32tDcfIhHUw">http://www.bccdc.ca/health-<wbr>info/diseases-conditions/<wbr>covid-19/covid-19-and-children</a></em>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnBoxedTextContentContainer">
		<tbody>
		<tr>
			<td>
				<table class="m_7160148765403798431mcnTextContentContainer">
				<tbody>
				<tr>
					<td><strong>14. COVID-19: BC Dashboard</strong>
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><strong>Click on the links or image below to access the BC COVID-19 dashboard for the latest case counts and information: </strong>
				</p>
				<p>Mobile and most browsers:
				</p>
				<p><a href="https://experience.arcgis.com/experience/a6f23959a8b14bfa989e3cda29297ded" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://experience.arcgis.com/experience/a6f23959a8b14bfa989e3cda29297ded&source=gmail&ust=1603482362051000&usg=AFQjCNGQrg6OAFsMM59BmWRZxNvyIdRyKw">https://experience.arcgis.com/<wbr>experience/<wbr>a6f23959a8b14bfa989e3cda29297d<wbr>ed</a>
				</p>
				<p>Internet Explorer users:
				</p>
				<p><a href="https://governmentofbc.maps.arcgis.com/apps/opsdashboard/index.html#/11bd9b0303c64373b5680df29e5b5914" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://governmentofbc.maps.arcgis.com/apps/opsdashboard/index.html%23/11bd9b0303c64373b5680df29e5b5914&source=gmail&ust=1603482362051000&usg=AFQjCNFURxhxGzZUWCAIf-_QMRtYJfzjmw">https://governmentofbc.maps.<wbr>arcgis.com/apps/opsdashboard/<wbr>index.html#/<wbr>11bd9b0303c64373b5680df29e5b59<wbr>14</a>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><strong>Click on the links or image below to access the BC COVID-19 dashboard for the latest case counts and information: </strong>
				</p>
				<p>Mobile and most browsers:
				</p>
				<p><a href="https://experience.arcgis.com/experience/a6f23959a8b14bfa989e3cda29297ded" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://experience.arcgis.com/experience/a6f23959a8b14bfa989e3cda29297ded&source=gmail&ust=1603482362052000&usg=AFQjCNGNuF6Br4UOIxONeyn9vDWPKeWYcg">https://experience.arcgis.com/<wbr>experience/<wbr>a6f23959a8b14bfa989e3cda29297d<wbr>ed</a>
				</p>
				<p>Internet Explorer users:
				</p>
				<p><a href="https://governmentofbc.maps.arcgis.com/apps/opsdashboard/index.html#/11bd9b0303c64373b5680df29e5b5914" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=https://governmentofbc.maps.arcgis.com/apps/opsdashboard/index.html%23/11bd9b0303c64373b5680df29e5b5914&source=gmail&ust=1603482362052000&usg=AFQjCNF8J-w_XxzFJ9nRqlu7hYdqqa_C-w">https://governmentofbc.maps.<wbr>arcgis.com/apps/opsdashboard/<wbr>index.html#/<wbr>11bd9b0303c64373b5680df29e5b59<wbr>14</a>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
	<td>
		<table class="m_7160148765403798431mcnTextContentContainer">
		<tbody>
		<tr>
			<td>
				<p><strong>The South UPDATE is monthly or as needed. Let us know if you have information that you would like us to add for the wider community.</strong>
				</p>
				<p>Stay healthy and safe,
				</p>
				<p>Lee Saxell (she/her), RM, MA
				</p>
				<p>(Co-Director) on behalf of the South Community Birth Program Team<br>
				</p>
				<p>Rita McCracken (she/her), MD, PhD
				</p>
				<p>(Interim Medical Director) on behalf of the South Hill Health Centre Team
				</p>
				<p><em>We acknowledge that our clinics operate on the traditional, unceded territory of the Coast Salish peoples, including the Səl̓ílwətaʔ/Selilwitulh (Tsleil-Waututh), the xʷməθkwəy̓əm (Musqueam), and the Skwxwú7mesh (Squamish) Nations.</em>
				</p>
				<p><em>If you are experiencing an emergency, such as problems breathing, please call 911.  </em><br><em>If you have health concerns, call HealthLink BC at 8-1-1</em>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
</tr>
</tbody>
</table>

<h2>Covid Update archive</h2>
<ul>
	<li><a href="https://mailchi.mp/5fb253ae8709/motherlode-postpartum-workshops-1969794" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE September 2020</a></li>
	<li><a href="https://mailchi.mp/c04177b4210b/motherlode-postpartum-workshops-1969754" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE August 2020</a></li>
	<li><a href="https://mailchi.mp/cb286eb81406/motherlode-postpartum-workshops-1969678" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE July 2020</a></li>
	<li><a href="https://mailchi.mp/352392303544/motherlode-postpartum-workshops-1969622" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE June 6 Update</a></li>
	<li><a href="https://mailchi.mp/051d6cd17715/motherlode-postpartum-workshops-1969586" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE #12 May 23, 2020</a></li>
	<li><a href="https://mailchi.mp/c885cbe3c40a/motherlode-postpartum-workshops-1969533" target="_blank" rel="nofollow noopener">South Hill COVID-19 UPDATE #11 May 09, 2020</a></li>
	<li><a href="https://mailchi.mp/783344c40e42/motherlode-postpartum-workshops-1969485" target="_blank" rel="nofollow noopener">South Hill COVID-19 UPDATE #10 APRIL 25, 2020</a></li>
	<li><a href="https://mailchi.mp/c7bdf76ccce5/motherlode-postpartum-workshops-1969437" target="_blank" rel="nofollow noopener">SH REVISED EDITION COVID-19 UPDATE #8 APRIL 10, 2020</a></li>
	<li><a href="https://mailchi.mp/7631bdcd9688/motherlode-postpartum-workshops-1969425" target="_blank" rel="nofollow noopener">SH COVID-19 UPDATE APRIL 9, 2020</a></li>
	<li><a href="https://mailchi.mp/58b27c964096/motherlode-postpartum-workshops-1969394" target="_blank" rel="nofollow noopener">SH COVID-19 Virtual Appointments, Child Vaccine Clinics and Support Update #7 April 3, 2020</a></li>
	<li><a href="https://mailchi.mp/c43260f3fbc8/motherlode-postpartum-workshops-1969370" target="_blank" rel="nofollow noopener">SH COVID-19 Updates and Counselling Workshops March 28, 2020 Update # 6</a></li>
	<li><a href="https://mailchi.mp/e33280785c05/motherlode-postpartum-workshops-1969354" target="_blank" rel="nofollow noopener">SH COVID-19 Clinic Appointment Updates and Pregnancy Care Information March 20, 2020</a></li>
	<li><a href="https://mailchi.mp/351560eeb666/motherlode-postpartum-workshops-1969342" target="_blank" rel="nofollow noopener">SH COVID-19: Important Appointment Changes and Clinic Updates</a></li>
	<li><a href="https://mailchi.mp/f1c2f2db74b2/motherlode-postpartum-workshops-1969326" target="_blank" rel="nofollow noopener">SH COVID-19 Update #3 – March 15, 2020 Information for Pregnancy & Birth</a></li>
	<li><a href="https://mailchi.mp/3219d547afa6/motherlode-postpartum-workshops-1969314" target="_blank" rel="nofollow noopener">SH COVID-19 Update #2 – March 14, 2020</a></li>
	<li><a href="https://mailchi.mp/3ecf22f8fb43/motherlode-postpartum-workshops-1969302" target="_blank" rel="nofollow noopener">SH Clinic Patient Announcement RE: COVID-19 Update</a></li>
</ul>
