---
title: Patient History
is_hidden: true
masthead_image: /assets/img/background/play-area.jpg
not_accepting_patients: |
  <p>Thank you for your interest in becoming a patient at South Hill. At this time we are not accepting new patients.
  </p>
template: history
fieldset: register
id: 6b863b5b-9021-499f-830c-177d03609db2
---
<p>Welcome to South Hill! We'd like you to complete this history form before your first appointment. This website is secure and your medical information will go directly to your Electronic Medical Record and will not be shared with anyone. Having this information in advance is very helpful for the doctor or nurse practitioner that you will be seeing at your first visit.
</p>