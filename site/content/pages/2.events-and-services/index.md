title: 'Events & Services'
masthead_image: /assets/img/background/masks.jpg
clinic_services:
  - 
    title: Counselling Support
    content: |
      <p>Danielle Schroeder Registered Clinical Counsellor, MA, RCC
      </p>
      <p>I have been a registered clinical counsellor since 2008. For the first ten years of my career I worked primarily with the Callanish Society, counselling patients through trauma, cancer, end of life, and grief. I’ve also counselled a broad range of individuals, couples, and families in private practice.
      </p>
      <p>More recently, my own experience of childbirth and transitioning to parenthood sparked my desire to support families going through this roller coaster stage of life. I am delighted to be joining the team at South Community Birth Program. Together, we offer a safe and supportive space for expecting mothers, their partners, and new parents to explore the myriad emotions that accompany this profound experience. I look forward to working with you and learning from one another.
      </p>
      <p>Counselling Supports
      </p>
      <p>Coping with grief after the loss of a pregnancy or baby
      </p>
      <p>Birth Trauma
      </p>
      <p>Anxiety & Depression
      </p>
      <p>Preparing emotionally for childbirth
      </p>
      <p>Relationship counselling
      </p>
      <p>Adjustment to changing identities of parenthood
      </p>
      <p>To book a counselling session, please visit <a href="https://oab.owlpractice.ca/danielleschroeder/1/calendar">https://oab.owlpractice.ca/danielleschroeder/1/calendar</a>
      </p>
      <p>*Evening appointments are available
      </p>
  - 
    title: Regular check ups
    content: |
      <p>We offer a full review of your health history and current issues, including cancer and chronic disease screening tests, and advice about lifestyle changes and prevention. These visits often include a full physical exam, however, not every patient needs to have a complete physical. Our Medical Services Plan in BC does not fund complete physicals for everyone. The doctor will review this with you at your appointment.
      </p>
  - 
    title: Joint injections
    content: |
      <p>We offer therapeutic joint injections for select conditions. These require a consultation with a physician before you book.
      </p>
  - 
    title: Mole removals and biopsies
    content: |
      <p>We can do simple biopsies and removals of certain skin lesions. You need to have a consultation with the doctor before booking this appointment. There are occasionally fees associated with these procedures.
      </p>
  - 
    content: |
      <p>We offer IUD insertions and have a fairly high volume of these at our clinic. You need to discuss this with a doctor or NP prior to your insertion, and need to have a prescription so you can bring your IUD to your appointment with you.
      </p>
      <p>For more information on IUD’s:
      </p>
      <p><a href="https://www.optionsforsexualhealth.org/birth-control-pregnancy/birth-control-options/iuds">https://www.optionsforsexualhealth.org/birth-contr...</a>
      </p>
    title: IUD insertions
  - 
    title: Pap tests
    content: |
      <p>We offer routine pap test screening according to the guidelines set out by the BC Cancer agency.
      </p>
      <p>For more information on pap screening go to:
      </p>
      <p><a href="http://www.screeningbc.ca/Cervix/default.htm">http://www.screeningbc.ca/Cervix/default.htm</a>
      </p>
  - 
    title: Vaccinations
    content: |
      <p>We offer routine infant check ups and vaccinations, up until the 18 month vaccines. We ask you to book your child’s 4-6 year boosters with public health or through the school. We can also do vaccines and boosters for older children, adolescents and adults, but you should check with us to be sure we have the vaccine that you need. Some vaccines do need to be paid for, if they are not covered by public health. For travel advice and vaccines we encourage you to visit your local travel clinic. We hold yearly flu shot clinics.
      </p>
      <p>For more information please go to:
      </p>
      <p><a href="http://immunizebc.ca/">http://immunizebc.ca/</a>
      </p>
  - 
    title: Urgent same day appointments
    content: |
      <p>We offer same day booking for urgent issues. Your doctor or NP holds spots every day for urgent concerns. However, if your primary care provider is not available, you are welcome to see another doctor or NP. Please try to book with us before going to a walk in clinic. We cannot guarantee that we can see you, but we try very hard to take care of you! These are 10 min appointments and the doctor will only be addressing the urgent concern.
      </p>
  - 
    title: Doctor’s notes and medical forms
    content: |
      <p>Doctor’s notes and medical forms are not covered by your medical plan. We charge a very reasonable fee for these forms, and the fee depends on the length and complexity of the form.
      </p>
template: services
fieldset: services
id: 4ae3d7c4-dae3-464f-bc98-a51deeb39917
