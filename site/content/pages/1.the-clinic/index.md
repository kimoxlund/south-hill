---
title: The Clinic
masthead_image: /assets/img/background/meeting-room.jpg
gallery:
  - 
    gallery_image: /assets/img/background/reception.jpg
  - 
    gallery_image: /assets/img/background/meeting-room.jpg
  - 
    gallery_image: /assets/img/background/mural.jpg
  - 
    gallery_image: /assets/img/background/play-area.jpg
clinic_policies:
  - 
    title: Scheduling appointments
    content: |
      <p>We discourage any switching of providers at South Hill and ask that you book in with the provider that you have been assigned to whenever possible. If you do not know who that is, you are welcome to ask!
      </p>
      <p>There are several ways to make an appointment.
      </p>
      <p>We encourage our patients to self-book online. Please note that we are not a walk-in clinic, and we can only see patients that are registered with us.  You can access the online booking <a href="https://veribook.com/lp/SouthHill/">here</a>, which includes same day appointments. However, if you would prefer direct contact with the clinic, please note the following:
      </p>
      <p>For same day appointments: please email us with the patient's name, reason for coming in, and approximate times in the day that work with your schedule.  These are first come first serve, and we do not guarantee that they can be seen that day.  IF YOU EMAIL US, PLEASE NOTE THAT WE WILL BE EMAILING YOU BACK WITH AN APPOINTMENT AND IT WILL BE CONSIDERED CONFIRMED. WE WILL NOT BE CALLING YOU.
      </p>
      <p>For all other appointments: please call the front desk at <a href="tel:604-428-8878">604-428-8878</a>.
      </p>
      <p><br>
      </p>
  - 
    title: Late arrivals and missed appointments
    content: |
      <p>We respect all our patients’ time and always do our best to keep our clinic running on schedule. We know your time is important and we don’t like to keep you waiting. In order to keep our clinic running smoothly, we need you to arrive on time.
      </p>
      <p>If you are coming for well baby/vaccine visit, you need to arrive <strong>15 minutes </strong>early to get your child’s growth measurements done in time to review with the doctor or nurse practitioner. If you are coming for an IUD insertion, complete physical exam or prenatal check up, please arrive at least <strong>10 minutes </strong>early to do some testing before your appointment starts.
      </p>
      <p>Your visit will need to be cut short if you arrive late. Provided that the doctor you are seeing has time, you may be given a 5 minute, squeeze-in appointment. If you arrive more than 10 minutes late you will need to re-schedule and will be billed the $40 missed appointment fee.
      </p>
      <p><span></span>We will try to address as many concerns as we can safely and appropriately manage in one appointment. This means that if you have a number of issues to review, we may have to prioritize them and book another appointment to address them all.
      </p>
      <p><strong>We require 24 hours cancellation notice for all appointments.</strong>
      </p>
      <p>If you do not call to cancel your appointment within 24 hours or do not show up for your booked appointment, you will be charged a cancellation fee of $40.  The cancellation fee still applies for same day appointments that are booked and subsequently cancelled. If you book an appointment that requires 30 minutes or longer, you will be charged $100 for a cancellation under 24 hours or no show.
      </p>
      <p><strong>After three ‘no shows’ we reserve the right not to schedule another appointment.</strong>
      </p>
  - 
    title: Referrals, prescriptions and test results
    content: |
      <p>If you require a prescription refill, referral to a specialist or if you want to discuss your test results, you need to make an appointment. If you require an emergency prescription refill and can’t come into the clinic, we will provide a short term refill for a fee of $30.  If you have misplaced your prescription and require another copy, we charge $12.50 for a replacement.
      </p>
      <p>Non-urgent test results will not be discussed by phone. The doctors and nurse practitioner are very busy during clinic days and are not available to take phone calls. If you have a concern that is non-urgent and you cannot come in to clinic, consider calling the nurses line at 8-1-1.
      </p>
  - 
    title: Services not covered by MSP
    content: |
      <p>There are some clinic services that are NOT covered by your MSP. These include:
      </p>
      <ul>
      <li>Doctor’s note – $20</li>
      <li>Short medical form – $35</li>
      <li>Long medical form – $35 for the first page; each subsequent page additional $20 each</li>
      <li>Treatment of skin tags – 3 or less – $30, More than 3 – $50</li>
      <li>Mole removal for cosmetic reasons – $150</li>
      <li>Physicals (for those &lt;50 years of age) - $100</li>
      </ul>
      <p>If you do not have valid MSP coverage at the time of your appointment, we charge $75 for a short private pay visit (&lt;20 minutes) and $100 for a longer visit (30 minutes).
      </p>
      <p>If you want a copy of your health records we will download them onto a USB stick or transfer them electronically to another clinic. The cost is $25 per patient. We do not print off copies of your records for environmental reasons.
      </p>
  - 
    title: Medical-legal Reports
    content: |
      <p>We do charge a fee for medical legal reports and forms, unless these are already covered by your insurance or another party. The amount of the fee depends on the length of the form, but these are in line with the Doctors of BC provincial recommendations.
      </p>
  - 
    title: WorksafeBC claims
    content: |
      <p>We are able to provide WorksafeBC related services to our patients. Please have all the details and information regarding your claim at your first visit. There may be associated fees for some forms.
      </p>
  - 
    title: Insurance Corporation of British Columbia (ICBC) claims
    content: |
      <p>We are able to see our patients for ICBC related injuries. Please have all the details and information regarding your claim at your first visit. There may be associated fees for some forms.
      </p>
template: clinic
fieldset: clinic
id: e2085621-37fe-4088-9f31-63387bfa1a51
---
<p>We are passionate about patient-centered health care! You and your family will have a lead physician or a nurse practitioner who will be your primary care giver. However, for urgent medical concerns requiring a same or next day appointment, you are welcome to see another doctor or NP. We also have nurses, clinical counselors, and other health care professionals providing care at South Hill.
</p>
<p>We are generally open Monday - Friday, 9am-5pm. We also offer Saturday clinics at least once a month; the schedules vary. We work hard to offer same day appointments for urgent health concerns. Email us or <a href="https://veribook.com/lp/SouthHill/">book an appointment online</a> to make an appointment the same day.
</p>
<p>We are proud to be a teaching site for Family Medicine residents, Nurse Practitioner and Midwifery students. Occasionally you will see a kind and competent learner during your visit, and we thank you in advance for seeing them.
</p>
<p>South Hill Health Centre emphasizes patient empowerment, education, and choice. We also understand that health is more than the absence of illness and offer workshops such as yoga, nutrition, mindfulness/self-care, and parenting.
</p>