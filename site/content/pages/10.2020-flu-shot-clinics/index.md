---
title: 2020 Flu shot clinics
is_hidden: false
masthead_image: /assets/img/background/play-area.jpg
template: flushot
fieldset: page
id: 0c6f4d2c-5d57-4333-b0b7-1330e7b01fa8
---
<p>A limited number of flu vaccines will be available for our patients starting <em>Tuesday, October 13th to Friday, October 30. </em>Priority is given to families with children from 6 months to 5 years of age. The family will all come together for their flu shots. Children under the age of 9 who have never had a flu vaccine, need 2 doses, 4 weeks apart, for sufficient protection. We are also prioritizing seniors 65 years of age or older and patients at risk with chronic health issues.
</p>
<p>The Flu Shot clinics will be held <strong>Monday to Friday from 9am-12pm & 2pm-5pm</strong>
</p>
<p>You must fill the consent form below to book an appointment. Please do not phone to book a flu shot appointment.
</p>
<p>Specify if you want to come between 9am and 12pm or 2pm and 5pm.<em>You cannot pick your exact time.</em> Your appointment will be confirmed by email.
</p>
<p><strong>We will email you back with a specific time and instructions for your appointment.</strong>
</p>
<p>When you arrive at the clinic for your appointment:
</p>
<ul>
	<li><strong>Dress in short sleeves </strong>under a jacket allowing for easy access to your upper arm</li>
	<li>All participants will be asked to wear a mask</li>
	<li>Please practice social distancing</li>
	<li>If you arrive late to your appointment we will take the next person<strong></strong></li>
</ul>