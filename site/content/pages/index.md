---
title: An Innovative Clinic for Primary Care Family Practice
is_hidden: false
masthead_image: /assets/img/background/reception.jpg
template: home
fieldset: page
id: db0ae4e3-4f10-4802-bc40-0b880cbf02c7
---
<p>South Hill Family Health Centre is an innovative new Family Practice clinic located in the South Vancouver area. Our goal is to provide ongoing, comprehensive health care, with a focus on families. We prioritize new patients who do not currently have a family doctor or nurse practitioner.<br>
</p>
<p>Our care is <strong>inter-professional</strong> and <strong>team based</strong> - with family doctors, nurse practitioners, nurses and clinical counselors.
</p>
<p>Our care is <strong>patient-centered</strong> - we are committed to our patients’ informed choices regarding their healthcare.
</p>
<p>Our care is <strong>accessible</strong> - we reserve spots for same day appointments and offer group medical visits.
</p>
<p>We host regular workshops for our patients - including nutrition, mindfulness meditation, and parenting/family dynamics.
</p>
<p><strong>UPDATE MARCH 23, 2020 </strong>
</p>
<p>A quick update for you! As we try many ways to reach you with our new appointments by phone or telehealth , we suggest you help us out by:
</p>
<p>  -Watching for regular email updates from South Hill
</p>
<p>-Watching your spam folder for a message from a care-provider with a "no-return" email address. (their name should be recognizable)
</p>
<p> -Listening to phone messages from a blocked phone number. It might be one of our providers telling you they are going to call you again soon or want to give you a time for your next telephone or in clinic appointment.
</p>
<p> Thanks so much<br>
</p>
<p>***
</p>
<p><strong>Dr. Hashini Bandaranayake </strong>has moved away and will not be returning to South Hill.  We are in the process of finding a doctor to cover her patients.  In the meantime, please continue booking with <strong>Dr. Danielle Chard, </strong>who is covering her patients during the interim.
</p>