---
title: Book Appointment
is_hidden: false
masthead_image: /assets/img/background/reception.jpg
fieldset: page
template: page
id: 2e4959c5-2899-4b26-b0fe-dbb15788cb97
---
<p><strong>ALL APPOINTMENTS ARE BEING DONE BY PHONE unless you have been otherwise notified. If you book an appointment through our online booking system your doctor will call you as close to the allotted time as possible. Please do not show up to the clinic.  </strong><br>
</p>
<p><br>
</p>
<p><strong>We now have a new online booking platform (Veribook). Please visit the following website to book your appointment online:</strong><br>
</p>
<p><strong><a href="https://veribook.com/lp/SouthHill/">https://veribook.com/lp/SouthHill/</a><span class="redactor-invisible-space"><br></span></strong>
</p>
<p><strong>Please note that if you book your appointment online, you are also able to cancel your appointment online (use the link shown on the confirmation that you will receive after booking an appointment)</strong><br>
</p>
<p><br>
</p>
<p>We do provide email reminders for appointments if you have given us your email address, but you are responsible for checking and confirming your appointment time. We charge a fee for missed appointments or cancellations with less than 24 hours notice. <strong> If you are unable to cancel your appointment online please call the clinic at 604-428-8878 or email us to cancel the appointment at south.families@gmail.com. </strong>
</p>
<p>Please note that Dr. Karina Zeidler is not accessible through the online booking at this time. If you are a patient of Dr. Zeidler, you need to call the clinic to book an appointment.
</p>
<p><br>
</p>
<p><br>
</p>