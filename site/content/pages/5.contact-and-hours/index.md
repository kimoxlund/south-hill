---
title: 'Contact & Hours'
is_hidden: false
masthead_image: /assets/img/background/mural.jpg
template: contact
fieldset: page
id: 566e824c-60d8-4e04-abc7-a08b6e3447ef
---
<p><strong>UPDATE MARCH 23, 2020</strong>
</p>
<p>A quick update for you! As we try many ways to reach you with our new appointments by phone or telehealth , we suggest you help us out by:
</p>
<p>-Watching for regular email updates from South Hill
</p>
<p>-Watching your spam folder for a message from a care-provider with a "no-return" email address. (their name should be recognizable)
</p>
<p>-Listening to phone messages from a blocked phone number. It might be one of our providers telling you they are going to call you again soon or want to give you a time for your next telephone or in clinic appointment.
</p>
<p>Thanks so much
</p>
<p><strong>South Hill Family Health Centre</strong><br>
</p>
<p><a href="https://goo.gl/maps/gLs4de4fV6M2" rel="noopener"><strong>202 - 1193 Kingsway, <br>Vancouver, BC, <br>Canada</strong></a>
</p>
<p>Phone: <strong><a href="tel:604-428-8878">604-428-8878</a> </strong><br>Fax: <strong>604-428-8871</strong>
</p>
<p>Mon - Thu: <strong>9am - 5pm</strong><br>Fri: <strong>9am - 4pm</strong><br>Sat: <strong>Dates vary</strong><br>Sun: <strong>Closed</strong><br>
</p>
<p>The phone lines are very busy during the day. Consider sending us an email through the form below to avoid the phone tag!
</p>
<p>If you have a medical emergency, please call 9-1-1 or go to the closest emergency room. If you have an urgent medical concern, please call the clinic or email us to request an urgent clinic appointment for the next clinic day.
</p>
<p>If you have a question after the clinic is closed that can't wait until tomorrow, you can call the nurses line at 8-1-1, available 24 hours a day.
</p>
<p>For more information about after-hours access, call the clinic and listen to the instructions on the voicemail.
</p>