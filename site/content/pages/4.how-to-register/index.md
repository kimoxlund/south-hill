---
title: How To Register
masthead_image: /assets/img/background/toys.jpg
not_accepting_patients: |
  <p>Thank you for your interest in becoming a patient at South Hill Health Centre. At this time we are not accepting new patients.
  </p>
  <p><strong>Due to high demand and numerous requests daily, we do not wait-list potential patients. </strong> Please call your local Division of Family Practice to inquire about being placed on their patient list or check back on our website to see if our registration form is available.<br>
  </p>
  <p>If you are currently pregnant and seeking medical care, you may want to consider our sister program South Community Birth Program and register online at <a href="http://www.scbp.ca/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&q=http://www.scbp.ca/&source=gmail&ust=1498246012267000&usg=AFQjCNGw1_kdkjRnYNp-6ki0sagjnz_ghg">www.scbp.ca</a>.
  </p>
  <p><br>
  </p>
  <p><br>
  </p>
  <p><br>
  </p>
template: register
fieldset: register
id: 8b61cbee-2230-4ac3-980c-445ee987b4bb
---
<p>We are pleased to announce that we are accepting new patients again. We prioritize patients who have no family doctor and are residents of Vancouver. If you already have a doctor or live outside of Vancouver, we can NOT guarantee that you will be accepted into this clinic.
</p>
<p>We are receiving hundreds of new patient requests every week. As we can only book a limited number of new patient appointments every week, you can expect at least a <strong>6-8 week wait</strong> to have your first appointment once you have completed our registration form. We will connect with you via email once we can accept you as a patient.
</p>
<p>While you are waiting for your first appointment with us, please go to your local walk-in if you have medical concerns. If you or your infant needs immunizations, call your nearest public health unit. We will get your records from these clinics when you become a patient of our clinic.
</p>
<p>Please note, that Dr.Karina Zeidler is only accepting trans patients who are referred from CWHC. Please email instead of applying online if you have been referred from CWHC.
</p>