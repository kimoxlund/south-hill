// (function () {
//   "use strict";

//   // Cache name definitions
//   var cacheNameStatic = "static-v1";
//   var cacheNameTypekit = "typekit-v1";

//   var currentCacheNames = [
//     cacheNameStatic,
//     cacheNameTypekit
//   ];


//   // A new ServiceWorker has been registered
//   self.addEventListener("install", function (event) {
//     event.waitUntil(
//       caches.open(cacheNameStatic)
//         .then(function (cache) {
//           return cache.addAll([
//             "/site/themes/southhill/dist/js/main.min.js"
//           ]);
//         })
//     );
//   });


//   // A new ServiceWorker is now active
//   self.addEventListener("activate", function (event) {
//     event.waitUntil(
//       caches.keys()
//         .then(function (cacheNames) {
//           return Promise.all(
//             cacheNames.map(function (cacheName) {
//               if (currentCacheNames.indexOf(cacheName) === -1) {
//                 return caches.delete(cacheName);
//               }
//             })
//           );
//         })
//     );
//   });


//   // The page has made a request
//   self.addEventListener("fetch", function (event) {
//     var requestURL = new URL(event.request.url);

//     event.respondWith(
//       caches.match(event.request)
//         .then(function (response) {

//           if (response) {
//             return response;
//           }

//           var fetchRequest = event.request.clone();

//           return fetch(fetchRequest).then(
//             function (response) {

//               var shouldCache = false;

//               if (response.type === "basic" && response.status === 200) {
//                 shouldCache = cacheNameStatic;
//               } else if (response.type === "opaque") {
//                 if (requestURL.hostname.indexOf(".typekit.net") > -1) {
//                   shouldCache = cacheNameTypekit;
//                 } else {
//                   // just let response pass through, don"t cache
//                 }

//               }

//               if (shouldCache) {
//                 var responseToCache = response.clone();

//                 caches.open(shouldCache)
//                   .then(function (cache) {
//                     var cacheRequest = event.request.clone();
//                     cache.put(cacheRequest, responseToCache);
//                   });
//               }

//               return response;
//             }
//           );
//         })
//     );
//   });

// })();