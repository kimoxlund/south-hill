module.exports = function(grunt) {

  // Configurable paths
  var config = {
    theme: 'site/themes/southhill/'
  };

  grunt.initConfig({

    // Project settings
    config: config,

    pkg: grunt.file.readJSON('package.json'),

    watch: {
      grunt: { files: ['Gruntfile.js'] },

      sass: {
        files: '<%= config.theme %>scss/**/*.scss',
        tasks: ['sass']
      },

      js: {
        files: '<%= config.theme %>js/*.js'
      },

      options: { livereload: true, } // Add Live Reload Chrome Extension for this to work
    },

    sass: {
      options: {
        sourceMap: true,
        sourceMapEmbed: true,
        sourceMapContents: true
      },
      dist: {
        options: {
          outputStyle: 'expanded'
        },
        files: {
          '<%= config.theme %>css/main.css': '<%= config.theme %>scss/main.scss'
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 9']
      },
      dist:{
        files:{
          '<%= config.theme %>css/main.css': '<%= config.theme %>css/main.css'
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      build: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.theme %>dist'
          ]
        }]
      },
      server: '.tmp'
    },

    copy: {
      build: {
        files: [{
          expand: true,
          cwd: '<%= config.theme %>',
          src: ['img/**'],
          dest: '<%= config.theme %>dist/',
        }],
      },
    },

    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [
          '<%= config.theme %>js/vendor/jquery.js',
          '<%= config.theme %>js/vendor/happy.js',
          '<%= config.theme %>js/vendor/areyousure.js',
          '<%= config.theme %>js/vendor/lazysizes.js',
          '<%= config.theme %>js/main.js'
        ],
        dest: '.tmp/js/main.js',
      }
    },

    uglify: {
      all: {
        files: [{
          expand: true,
          cwd: '.tmp/js/',
          src: ['*.js'],
          dest: '<%= config.theme %>dist/js/',
          ext: '.min.js'
        }],
      },
    },

    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: '<%= config.theme %>css/',
          src: ['*.css'],
          dest: '<%= config.theme %>dist/css/',
          ext: '.min.css'
        }]
      }
    },

    inline: {
      dist: {
        src: '<%= config.theme %>layouts/default.html'
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-inline');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('build', ['clean:build', 'copy', 'sass', 'autoprefixer', 'cssmin', 'inline', 'concat', 'uglify']);
  grunt.registerTask('default', ['build','watch']);
};
